Data Analytics Problemset
=========================

# Introduction #

Thanks for your interest in working at Welbi. We are really excited to get started and save lives.

The implementation described in this document will be considered your final interview (with a follow up meeting a few days after submission).

## The Problem ##

The problem that needs to be solved is a precursor to what you will potentially be working on in this position. Included is a CSV file of [my] Nick's health data. This is a classic example of what data looks like in production. You'll notice that lots of data is missing and some dates are skipped (I've removed some data). 

Your tasks will be to develop a model that establishes my normal habits, and will detect any new data that seems outside of normal. Your job is to replace any missing data (0's are considered missing data), tranform it and develop the model. The output can be in any format you like, as long as its understandable. All your code should be written in core.py, where I have loaded the dataset with Pandas and removed any useless data that you won't need. 

You will need to have > Python 2.7 (use only Python 2.$, no Python 3.$) installed, along with Pandas and Numpy. You can use any other additional libraries you want. 

You will be evaluated on the viability and elegance of your solution, along with the quality of your code.

## The Dataset ##

The dataset contains activity data, such as steps, distance (in meters), duration (in seconds), light, moderate, vigorous, total active minutes and sedentary activity (in minutes). Try to use as much of this data as you can in your solution.

All data is represented as a timeseries, every example is labeled with a Y-m-d formatted date.


## Submitting your Results ##

Submit a merge request with your solution before ### Friday, September 1st ###

### Thanks and Good Luck ###
import sys
import json

import math
import pandas as pd
import numpy as np

def main():

	# Make sure you have an up-to-date version of Python
	if sys.version_info <= (2,7):
		raise Exception('You don\'t have an up to date version of Python')

	# Load the CSV file as a Pandas dataframe
	dataframe = pd.read_csv('NzM1MjYwOWUxNTg4.csv')

	# Remove any unnecessary metadata
	dataframe = dataframe.drop([
		'patient_id', 'calories', 'human_id', 'source_created_at', 'source_data', 'source_updated_at'
	], axis=1, errors='ignore')

	#YOUR CODE GOES HERE
	# .......

if __name__ == '__main__':
	main()